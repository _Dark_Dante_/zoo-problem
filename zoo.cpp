#include <cstdio>
#include <algorithm>
#include <iostream>

const int MAXIMUM = 1000000;

long long end[MAXIMUM], mas[MAXIMUM], rev[MAXIMUM], start[MAXIMUM], array[MAXIMUM];

int main()
{
	int i, j, n;
	long long temp, minc = 6500, el_max, sum, result = 0;

	std::cin >> n;
	for (i = 1; i <= n; ++i)
	{
		std::cin >> mas[i];
		minc = std::min(minc, mas[i]);
	}
	for (i = 1; i <= n; ++i)
		std::cin >> start[i];
	for (i = 1; i <= n; ++i)
		std::cin >> end[i];
	for (i = 1; i <= n; ++i)
		array[start[i]] = end[i];
	for (i = 1; i <= n; ++i)
		if (!rev[i])
		{
			j = i;
			el_max = 6500;
			temp = 0;
			sum = 0;
			while (!rev[j])
			{
				el_max = std::min(el_max, mas[j]);
				rev[j] = 1;
				temp++;
				sum += (long long)mas[j];
				j = array[j];
			}
			result += (long long)std::min(sum + (temp - 2)*el_max, sum + el_max + (temp + 1)*minc);
		}
	std::cout << result;
	system("pause");
	return 0;
}
